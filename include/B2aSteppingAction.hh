//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B2aSteppingAction.hh 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file B2aSteppingAction.hh
/// \brief Definition of the B2aSteppingAction class

#ifndef B2aSteppingAction_h
#define B2aSteppingAction_h 1

#include "G4UserSteppingAction.hh"

//class B4DetectorConstruction;
//class B2aEventAction;

/// Stepping action class.
///
/// In UserSteppingAction() there are collected the energy deposit and track
/// lengths of charged particles in Absober and Gap layers and
/// updated in B2aEventAction.

class B2aSteppingAction : public G4UserSteppingAction
{
public:
  B2aSteppingAction();
  virtual ~B2aSteppingAction();

  virtual void UserSteppingAction(const G4Step* step);

private:
  //const B4DetectorConstruction* fDetConstruction;
  //B2aEventAction*  fEventAction;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
