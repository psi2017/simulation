import ROOT

ns = range(5,16,5) + range(25,375,25)
ns = range(25,376,25) + [130, 140, 160, 170, 180, 190]

p = []
for n in ns:
    print n
    f = ROOT.TFile("pi0_"+str(n)+".root")
    f.Get("pi0").Draw("pi0_px>>h")
    p.append(ROOT.gDirectory.Get("h").GetEntries())

import matplotlib.pyplot as plt

import numpy as np
p = np.array(p)
nn = 100000

print ns, p
plt.errorbar(ns, p/nn, np.sqrt(p)/nn, fmt='x')
plt.xlabel("momentum [MeV/c]")
plt.ylabel("pi0/pi-")
plt.grid()
plt.show()
