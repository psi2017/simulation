//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B2aSteppingAction.cc 68058 2013-03-13 14:47:43Z gcosmo $
//
/// \file B2aSteppingAction.cc
/// \brief Implementation of the B2aSteppingAction class

#include "B2aSteppingAction.hh"
#include "B2aAnalysis.hh"
//#include "B2aEventAction.hh"
//#include "B2aDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4DecayProducts.hh"

class G4DecayProducts;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2aSteppingAction::B2aSteppingAction()
  : G4UserSteppingAction()
    //fDetConstruction(detectorConstruction),
    //fEventAction(eventAction)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2aSteppingAction::~B2aSteppingAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2aSteppingAction::UserSteppingAction(const G4Step* step)
{
// Collect energy and track length step by step

  // get volume of the current step
  //G4VPhysicalVolume* volume
  //  = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
  //G4cout << "Volume: " << volume->GetName() << G4endl;
  //if(volume->GetName() == "Target") {
    //G4cout << "Particle: " << step->GetTrack()->GetDefinition()->GetParticleName() << G4endl;
    if(step->GetTrack()->GetDefinition()->GetPDGEncoding() == 111) {
      //G4cout << "pi0!!!!!" << step->GetTrack()->GetDefinition()->GetPDGEncoding() << G4endl;
      const G4TrackVector * sec = step->GetSecondary();
      G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
      auto pi0_p = step->GetTrack()->GetMomentum();
      analysisManager->FillNtupleDColumn(0, pi0_p.x());
      analysisManager->FillNtupleDColumn(1, pi0_p.y());
      analysisManager->FillNtupleDColumn(2, pi0_p.z());
      int ns = sec->size();
      if(ns == 2) {
        //G4cout << "pi0 -> gamma gamma" << G4endl;
        analysisManager->FillNtupleDColumn(3, 0);
      }
      if(ns == 3) {
        G4cout << "pi0 -> e+ e- gamma " << G4endl;
        analysisManager->FillNtupleDColumn(3, 1);
      }

      int ngamma = 0;
      for(auto * t : (*sec)) {
	//G4cout << t->GetDefinition()->GetPDGEncoding() << " ";
        auto t_p = t->GetMomentum();
        // gamma
        if(t->GetDefinition()->GetPDGEncoding() == 22) {
          ++ngamma;
          if(ngamma == 1) {
            analysisManager->FillNtupleDColumn(4, t_p.x()); // g_px
            analysisManager->FillNtupleDColumn(5, t_p.y()); // g_py
            analysisManager->FillNtupleDColumn(6, t_p.z()); // g_pz
          } else {
            analysisManager->FillNtupleDColumn(7, t_p.x()); // g2_px
            analysisManager->FillNtupleDColumn(8, t_p.y()); // g2_py
            analysisManager->FillNtupleDColumn(9, t_p.z()); // g2_pz
          }
        }
        // e+
        if(t->GetDefinition()->GetPDGEncoding() == -11) {
          analysisManager->FillNtupleDColumn(10, t_p.x()); // p_px
          analysisManager->FillNtupleDColumn(11, t_p.y()); // p_py
          analysisManager->FillNtupleDColumn(12, t_p.z()); // p_pz
        }
        // e-
        if(t->GetDefinition()->GetPDGEncoding() == 11) {
          analysisManager->FillNtupleDColumn(13, t_p.x()); // e_px
          analysisManager->FillNtupleDColumn(14, t_p.y()); // e_py
          analysisManager->FillNtupleDColumn(15, t_p.z()); // e_pz
        }
        //G4cout << "\t" << t->GetDefinition()->GetParticleName() << " " << t->GetDefinition()->GetPDGEncoding() << G4endl;
        //if(t->GetDefinition()->GetParticleName() == "gamma") ++ngamma;
      }
      //G4cout << G4endl;

      analysisManager->AddNtupleRow();

      /*G4cout << "Secondary tracks: " << ns << G4endl;
      int ngamma = 0;

      if(ns == 2 && ngamma == 2) {
        G4cout << "pi0 -> gamma gamma" << G4endl;
      }
      if(ns == 3 && ngamma == 1) {
        G4cout << "pi0 -> e+ e- gamma " << G4endl;
      }*/

      //const G4DecayProducts * dp = step->GetTrack()->GetDynamicParticle()->GetPreAssignedDecayProducts();
      //G4cout << "Decay particles: " << dp->entries() << G4endl;
      //G4DynamicParticle* part;
      //for(int i = 0; i < dp->entries(); ++i ) {
      //  part = (*dp)[i];
      //  G4cout << "\t" << i << ") " << part->GetDefinition()->GetParticleName() << G4endl;
      //}
    }
    /*else {
        if(step->GetTrack()->GetDefinition()->GetPDGEncoding() == -211) {
            const G4TrackVector * sec = step->GetSecondary();
            for(auto * t : (*sec)) {
                G4cout << t->GetDefinition()->GetParticleName() << " ";
            }
	    G4cout << G4endl;
	}
    }*/
  //

  // energy deposit
  //G4double edep = step->GetTotalEnergyDeposit();

  // step length
  //G4double stepLength = 0.;
  //if ( step->GetTrack()->GetDefinition()->GetPDGCharge() != 0. ) {
  //  stepLength = step->GetStepLength();
  //}


//  if ( volume == fDetConstruction->GetAbsorberPV() ) {
//    fEventAction->AddAbs(edep,stepLength);
//  }

//  if ( volume == fDetConstruction->GetGapPV() ) {
//    fEventAction->AddGap(edep,stepLength);
//  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
