//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: B2RunAction.cc 75214 2013-10-29 16:04:42Z gcosmo $
//
/// \file B2RunAction.cc
/// \brief Implementation of the B2RunAction class

#include "B2RunAction.hh"
#include "B2aAnalysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2RunAction::B2RunAction()
 : G4UserRunAction()
{
  // set printing event number per each 100 events
  G4RunManager::GetRunManager()->SetPrintProgress(1000);

  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  analysisManager->SetFirstHistoId(1);

// Book histograms, ntuple
//

// Creating histograms
//analysisManager->CreateH1("1","Edep in absorber", 100, 0., 800*MeV);

// Creating ntuple
//
  analysisManager->CreateNtuple("pi0", "pi0");
  analysisManager->CreateNtupleDColumn("pi0_px");
  analysisManager->CreateNtupleDColumn("pi0_py");
  analysisManager->CreateNtupleDColumn("pi0_pz");
  analysisManager->CreateNtupleDColumn("pi0_decay");
  analysisManager->CreateNtupleDColumn("g_px");
  analysisManager->CreateNtupleDColumn("g_py");
  analysisManager->CreateNtupleDColumn("g_pz");
  analysisManager->CreateNtupleDColumn("g2_px");
  analysisManager->CreateNtupleDColumn("g2_py");
  analysisManager->CreateNtupleDColumn("g2_pz");
  analysisManager->CreateNtupleDColumn("p_px");
  analysisManager->CreateNtupleDColumn("p_py");
  analysisManager->CreateNtupleDColumn("p_pz");
  analysisManager->CreateNtupleDColumn("e_px");
  analysisManager->CreateNtupleDColumn("e_py");
  analysisManager->CreateNtupleDColumn("e_pz");
  analysisManager->FinishNtuple();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

B2RunAction::~B2RunAction() {
  delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::BeginOfRunAction(const G4Run* run)
{
  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);

  // Get analysis manager
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  //
  int runID = run->GetRunID();
  G4String fileName = "pi0_"+std::to_string(runID);
  analysisManager->OpenFile(fileName);
  G4cout << "Start run " << runID << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void B2RunAction::EndOfRunAction(const G4Run* ){
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->Write();
  analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
